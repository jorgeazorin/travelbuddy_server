package com.travelbuddy.dao.db.config;

import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import javax.sql.rowset.serial.SerialBlob;

public abstract class AbstractDatabaseCommand {

    public class Contador{
        public Contador(Integer count){
            this.count = count;
        }
        public Integer count;
    }
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-M");
	
    public void execute() {
        try {
        	Connection conn = OracleThreadContext.context.get();
        	if (conn == null) {
        		conn = OracleConnection.INSTANCE.getInstance();
        	}
			this.performExecute(conn);
			if (conn.getAutoCommit()) {
				OracleThreadContext.close();
			}
		} catch (SQLException e) {
			OracleThreadContext.close();
		} catch (Exception e) {
			OracleThreadContext.close();
			throw e; 
		}
    }

    protected abstract void performExecute(final Connection connection);

    protected void closeConnection() {
    	OracleThreadContext.close();
    }




    protected Boolean setNullIfNull = false;


    public void setPreparedStatementNull(PreparedStatement preparedStatement, Contador parameterIndex) throws SQLException {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, Integer integer, Contador parameterIndex) throws SQLException {
        if(integer != null) {
            preparedStatement.setInt(parameterIndex.count, integer);
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, Boolean integer, Contador parameterIndex) throws SQLException {
        if(integer != null) {
            preparedStatement.setBoolean(parameterIndex.count, integer);
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, Double doubled, Contador parameterIndex) throws SQLException {
        if(doubled != null) {
            preparedStatement.setDouble(parameterIndex.count, doubled);
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, Long integer, Contador parameterIndex) throws SQLException {
        if(integer != null) {
            preparedStatement.setLong(parameterIndex.count, integer);
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, java.sql.Date integer, Contador parameterIndex) throws SQLException {
        if(integer != null) {
            preparedStatement.setDate(parameterIndex.count, integer);
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, Calendar calendar, Contador parameterIndex) throws SQLException {
        if(calendar != null) {
            preparedStatement.setTimestamp(parameterIndex.count, new Timestamp(calendar.getTimeInMillis()));
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, String s, Contador parameterIndex) throws SQLException {
        if(s != null) {
            preparedStatement.setString(parameterIndex.count, s);
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }

    public void setPreparedStatement(PreparedStatement preparedStatement, Blob s, Contador parameterIndex) throws SQLException {
        if(s != null) {
            preparedStatement.setBlob(parameterIndex.count, s);
            parameterIndex.count++;
        }else if (setNullIfNull) {
            preparedStatement.setNull(parameterIndex.count, Types.NULL);
            parameterIndex.count++;
        }
    }











    public void closeResult(final ResultSet result) {
        try {
        	if (result != null) {
        		result.close();
        	}
        } catch (Exception rse) {
            throw new RuntimeException("Error closing result. " + rse.getMessage());
        }
    }

    public void closePreparedStatement(final PreparedStatement preparedStatement) {
        try {
        	if (preparedStatement != null) {
        		preparedStatement.close();
        	}
        } catch (Exception sse) {
            throw new RuntimeException("Error closing preparedStatement. " + sse.getMessage());
        }
    }

    public void closeStatement(final Statement statement) {
        try {
            statement.close();
        } catch (Exception sse) {
            throw new RuntimeException("Error closing statement. " + sse.getMessage());
        }
    }

    public Long getSafeLong(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        return result.getLong(columnIndex);
    }

    public Short getSafeShort(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        return result.getShort(columnIndex);
    }
    
    public Double getSafeDouble(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        return result.getDouble(columnIndex);
    }

    public Integer getSafeInt(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        return result.getInt(columnIndex);
    }


    public Calendar getSafeCalendar(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(result.getTimestamp(columnIndex));
        return c;
    }


    public Float getSafeFloat(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        return result.getFloat(columnIndex);
    }

    public BigDecimal getSafeBigDecimal(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        return result.getBigDecimal(columnIndex);
    }
    
    public Boolean getSafeBoolean(final ResultSet result, final int columnIndex) throws SQLException {
        if (result.getObject(columnIndex) == null) {
            return null;
        }
        return result.getBoolean(columnIndex);
    }

    public Calendar toCalendar(final Date date) throws SQLException {
        if (date == null) {
            return null;
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }
    
    protected Calendar toCalendar(int year, int month) throws ParseException {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(SDF.parse(year+"-"+month));
		return calendar;
	}

    protected void appendToSql(final StringBuffer sb, final String query, final Object value) {
        if (value == null) {
            return;
        }
        sb.append(query);
    }

    protected void appendToSql(final StringBuffer sb, final String query, final Boolean value) {
        if (value == null || value.booleanValue() == false) {
            return;
        }
        sb.append(query);
    }
    
	protected String buildParams(List<?> paramList) {
		if (paramList == null) {
			return "";
		}
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < paramList.size(); i++) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append("?");
		}
		return sb.toString();
	}

    public java.sql.Date getSafeDate(DateTime dateTime) {
        if (dateTime != null) {
            return new java.sql.Date(dateTime.getMillis());
        }
        return null;
    }
    
    public java.sql.Date getSafeDate(Date date) {
        if (date != null) {
            return new java.sql.Date(date.getTime());
        }
        return null;
    }


}
