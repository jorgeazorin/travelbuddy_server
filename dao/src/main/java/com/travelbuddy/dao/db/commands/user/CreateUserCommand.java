package com.travelbuddy.dao.db.commands.user;

import com.travelbuddy.dao.db.config.AbstractDatabaseCommand;
import com.travelbuddy.domain.User;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.*;

public class CreateUserCommand  extends AbstractDatabaseCommand {

    private static final String SQL =
            " insert into travelbuddy.user  (email, password, name, lastname, birth, photo) values (?, ? , ?, ? ,?, ?)   ";


    private User user;




    public CreateUserCommand(User id) {
       this.user = id;
    }


    @Override
    protected void performExecute(Connection connection) {
        PreparedStatement preparedStatement = null;
        try {

            final StringBuffer sb = new StringBuffer(SQL);
            preparedStatement = connection.prepareStatement(sb.toString(), Statement.RETURN_GENERATED_KEYS);
            AbstractDatabaseCommand.Contador contador = new Contador(1);

            super.setNullIfNull = true;
            super.setPreparedStatement(preparedStatement, user.getEmail(), contador);
            super.setPreparedStatement(preparedStatement,user.getPassword(), contador);
            super.setPreparedStatement(preparedStatement, user.getName(), contador);
            super.setPreparedStatement(preparedStatement, user.getLastname(), contador);
            super.setPreparedStatement(preparedStatement, user.getBirth(), contador);
            if(user.getPhoto() != null)
                super.setPreparedStatement(preparedStatement, new SerialBlob(user.getPhoto()), contador);
            else{
                super.setPreparedStatementNull(preparedStatement, contador);
            }

            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        } finally {
            super.closePreparedStatement(preparedStatement);
        }
    }
}