package com.travelbuddy.dao.db.commands.buddy;

import com.travelbuddy.dao.db.config.AbstractDatabaseCommand;
import com.travelbuddy.domain.Buddy;
import com.travelbuddy.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetBuddyCommand extends AbstractDatabaseCommand {

    private static final String SQL =
            " select userId, publicName, location, description, hobbies "+
            "   from travelbuddy.buddy  where 1 = 1 ";

    private List<Buddy> buddies = new ArrayList<>();
    private final Params params;

    public GetBuddyCommand(Params params) {
        this.params = params;
    }


    public GetBuddyCommand(User user) {
        this.params = new Params();
        this.params.setUserId(user.getId());
    }

    public GetBuddyCommand() {
        this.params = new Params();
    }
    @Override
    protected void performExecute(Connection connection) {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {

            final StringBuffer sb = new StringBuffer(SQL);

            super.appendToSql(sb, " and userId = ? ", this.params.getUserId());

            preparedStatement = connection.prepareStatement(sb.toString());
            Contador contador = new Contador(1);

            super.setPreparedStatement(preparedStatement, params.getUserId(), contador);

            preparedStatement.execute();
            result = preparedStatement.getResultSet();


            while (result.next()) {
                int index =1;
                Buddy buddy = new Buddy();
                buddy.setUser(new User(super.getSafeInt(result, index++)));
                buddy.setPublicName(result.getString(index++));
                buddy.setLocation(result.getString(index++));
                buddy.setDescription(result.getString(index++));
                buddy.setHobbies(result.getString(index++));
                buddies.add(buddy);
            }

        } catch (SQLException e) {
            throw new RuntimeException( e.getMessage());
        } finally {
            super.closePreparedStatement(preparedStatement);
        }
    }



    public Buddy getBuddy(){
        if(buddies.size()>1){
            throw new RuntimeException("Se esperaba solo un buddy y se han encontrado más de uno");
        }else if(buddies.size()<1){
            throw new RuntimeException("No se han encontrado buddies");
        }
        return buddies.get(0);
    }


    public List<Buddy> getBuddies() {
        return buddies;
    }

    public static class Params{
        private Integer userId;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

    }
}