package com.travelbuddy.dao.db.commands.buddy;

import com.travelbuddy.dao.db.config.AbstractDatabaseCommand;
import com.travelbuddy.domain.Buddy;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;


public class UpdateBuddyCommand extends AbstractDatabaseCommand {

    private static final String SQL =
            " update travelbuddy.buddy set publicName = ?, location = ?, description = ?, hobbies = ?, photo = ? where userId = ?;  ";



    private Buddy buddy;
    public UpdateBuddyCommand(Buddy buddy) {
        this.buddy = buddy;
    }


    @Override
    protected void performExecute(Connection connection) {
        PreparedStatement preparedStatement = null;
        try {

            final StringBuffer sb = new StringBuffer(SQL);
            preparedStatement = connection.prepareStatement(sb.toString());
            Contador contador = new Contador(1);

            super.setNullIfNull = true;
            super.setPreparedStatement(preparedStatement, buddy.getPublicName(), contador);
            super.setPreparedStatement(preparedStatement, buddy.getLocation(), contador);
            super.setPreparedStatement(preparedStatement, buddy.getDescription(), contador);
            super.setPreparedStatement(preparedStatement, buddy.getHobbies(), contador);
            if(buddy.getPhoto() != null)
                super.setPreparedStatement(preparedStatement, new SerialBlob(buddy.getPhoto()), contador);
            else{
                super.setPreparedStatementNull(preparedStatement, contador);
            }

            super.setPreparedStatement(preparedStatement, buddy.getUser().getId(), contador);

            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        } finally {
            super.closePreparedStatement(preparedStatement);
        }
    }
}