package com.travelbuddy.dao.db.config;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public enum OracleConnection {
	INSTANCE;
	private final HikariDataSource cpds;
	private Lock connectionLock = new ReentrantLock();

	OracleConnection() {
		final HikariConfig config = new HikariConfig();
		config.setJdbcUrl("jdbc:mysql://travelbuddyparis.cqoozssy2msp.eu-west-3.rds.amazonaws.com:3306?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
		config.setDriverClassName("com.mysql.jdbc.Driver");
		config.setUsername("jorgeazorin");
		config.setPassword("Y7aR37gl+");
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

		this.cpds = new HikariDataSource(config);
	}

	/**
	 * Method to create a Connection on the oracle-database.
	 * 
	 * @return the Connection.
	 * @throws SQLException
	 */
	public Connection getInstance() throws SQLException {
		if (cpds == null) {
			return null;
		}
		connectionLock.lock();
		try {
			if (OracleThreadContext.context.get() == null) {
				OracleThreadContext.context.set(cpds.getConnection());
			}
		} finally {
			connectionLock.unlock();
		}
		
		final Connection conn = OracleThreadContext.context.get();
		
		//this.setTokenVariable(conn);

		return conn;
	}
	/*
	private void setTokenVariable(final Connection conn) throws SQLException {
	    if (ThreadContext.getInstance().getSession() == null || ThreadContext.getInstance().getSession().getToken() == null) {
	        return;
	    }
	
	    PreparedStatement preparedStatement = conn.prepareStatement("call logSetup(?, ?)");
        preparedStatement.setString(1, ThreadContext.getInstance().getSession().getToken());

        preparedStatement.executeUpdate();
	}
	*/

}