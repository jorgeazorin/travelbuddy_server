package com.travelbuddy.dao.db.config;

import java.sql.Connection;
import java.sql.SQLException;


public class OracleThreadContext {

	public static final ThreadLocal<Connection> context = new ThreadLocal<Connection>();
	
	
	public static void close() {
		
		try {
			if (context.get() != null) {
				context.get().close();
				context.set(null);
			}
		} catch (SQLException e) {
			throw new RuntimeException( e.getMessage());
		} catch (NullPointerException e) {
			throw new RuntimeException(e.getMessage());
		} catch (RuntimeException re) {
			throw new RuntimeException(re.getMessage());
		} finally {
		}
	}
	
}
