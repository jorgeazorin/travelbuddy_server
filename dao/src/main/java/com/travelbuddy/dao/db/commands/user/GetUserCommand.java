package com.travelbuddy.dao.db.commands.user;

import com.travelbuddy.dao.db.config.AbstractDatabaseCommand;
import com.travelbuddy.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetUserCommand  extends AbstractDatabaseCommand {

    private static final String SQL =
            " select id, email, password, name, lastname, birth "+
            "   from travelbuddy.user  where 1 = 1 ";

    private List<User> users = new ArrayList<>();
    private final Params params;

    public GetUserCommand(Params params) {
        this.params = params;
    }


    public GetUserCommand(Integer id) {
        this.params = new Params();
        this.params.setId(id);
    }

    public GetUserCommand() {
        this.params = new Params();
    }
    @Override
    protected void performExecute(Connection connection) {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {

            final StringBuffer sb = new StringBuffer(SQL);

            super.appendToSql(sb, " and id = ? ", this.params.getId());
            super.appendToSql(sb, " and email = ? ", this.params.getEmail());
            super.appendToSql(sb, " and password = ? ", this.params.getPassword());

            preparedStatement = connection.prepareStatement(sb.toString());
            AbstractDatabaseCommand.Contador contador = new Contador(1);

            super.setPreparedStatement(preparedStatement, params.getId(), contador);
            super.setPreparedStatement(preparedStatement, params.getEmail(), contador);
            super.setPreparedStatement(preparedStatement, params.getPassword(), contador);

            preparedStatement.execute();
            result = preparedStatement.getResultSet();


            while (result.next()) {
                int index =1;
                User user = new User();
                user.setId(super.getSafeInt(result,index++));
                user.setEmail(result.getString(index++));
                user.setPassword(result.getString(index++));
                user.setName(result.getString(index++));
                user.setLastname(result.getString(index++));
                user.setBirth(super.getSafeCalendar(result, index++));

                users.add(user);
            }


        } catch (SQLException e) {
            throw new RuntimeException( e.getMessage());
        } finally {
            super.closePreparedStatement(preparedStatement);
        }
    }



    public User getUser(){
        if(users.size()>1){
            throw new RuntimeException("Se esperaba solo un usuario y se han encontrado más de uno");
        }else if(users.size()<1){
            throw new RuntimeException("No se han encontrado usuarios");
        }
        return users.get(0);
    }


    public List<User> getUsers() {
        return users;
    }

    public static class Params{
        private Integer id;
        private String email;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        private String password;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

    }
}