package com.travelbuddy.dao.db.commands.user;

import com.travelbuddy.dao.db.config.AbstractDatabaseCommand;
import com.travelbuddy.domain.User;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateUserCommand extends AbstractDatabaseCommand {

    private static final String SQL =
            " update travelbuddy.user  set email = ?, password = ?, name = ?, lastname = ?, birth = ?, photo = ? where id = ?  ";


    private User user;
    public UpdateUserCommand(User user) {
       this.user = user;
    }


    @Override
    protected void performExecute(Connection connection) {
        PreparedStatement preparedStatement = null;
        try {

            final StringBuffer sb = new StringBuffer(SQL);
            preparedStatement = connection.prepareStatement(sb.toString());
            Contador contador = new Contador(1);

            super.setNullIfNull = true;
            super.setPreparedStatement(preparedStatement, user.getEmail(), contador);
            super.setPreparedStatement(preparedStatement,user.getPassword(), contador);
            super.setPreparedStatement(preparedStatement, user.getName(), contador);
            super.setPreparedStatement(preparedStatement, user.getLastname(), contador);
            super.setPreparedStatement(preparedStatement, user.getBirth(), contador);
            if(user.getPhoto() != null)
                super.setPreparedStatement(preparedStatement, new SerialBlob(user.getPhoto()), contador);
            else{
                super.setPreparedStatementNull(preparedStatement, contador);
            }

            super.setNullIfNull = false;
            super.setPreparedStatement(preparedStatement, user.getId(), contador);

            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        } finally {
            super.closePreparedStatement(preparedStatement);
        }
    }
}