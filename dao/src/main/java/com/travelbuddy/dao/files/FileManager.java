package com.travelbuddy.dao.files;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class FileManager {

    public enum Type{
        IMAGE_BUDDY, IMAGE_USER
    }



    public static void saveImage(Type type, InputStream file, Integer id){
        File dest = new File("C:/uploads/"+type.name()+"_"+id);
        try {
           FileUtils.copyInputStreamToFile(file, dest);
        }catch (Exception e){
        }
    }

    public static InputStream getImage(Type type, Integer id){
        File file = new File("C:/uploads/"+type.name()+"_"+id).getAbsoluteFile();
        try {
            return new FileInputStream(file);
        }catch (Exception e){
            return null;
        }
    }
}
