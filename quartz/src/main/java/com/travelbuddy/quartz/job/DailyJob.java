package com.travelbuddy.quartz.job;

import com.travelbuddy.processors.ExampleProcessor;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

public class DailyJob implements Job {

    public static boolean jobActive = false;

    public void execute(final JobExecutionContext context) {

        if(this.jobActive == false) {
            this.jobActive = true;
            try{
                ExampleProcessor example = new ExampleProcessor();
                example.execute();
            }catch (Exception e){

            }finally {
                this.jobActive = false;
            }
        }
    }

}