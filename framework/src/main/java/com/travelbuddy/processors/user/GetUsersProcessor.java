package com.travelbuddy.processors.user;


import com.travelbuddy.config.thread.ContextContainer;
import com.travelbuddy.dao.db.commands.user.GetUserCommand;
import com.travelbuddy.domain.User;
import com.travelbuddy.processors.config.AbstractRunnableProcessor;

import java.util.List;

public class GetUsersProcessor extends AbstractRunnableProcessor<List<User>> {

    private GetUserCommand.Params params;
    public GetUsersProcessor(ContextContainer context, GetUserCommand.Params params) {
        super(context);
        this.params = params;
    }

    @Override
    protected void execute() {

        GetUserCommand command = new GetUserCommand(params);
        command.execute();
        this.getDeferredResult().setResult(command.getUsers());
    }
}