package com.travelbuddy.processors.user;

import com.travelbuddy.config.security.TokenGenerator;
import com.travelbuddy.config.thread.ContextContainer;
import com.travelbuddy.dao.db.commands.user.GetUserCommand;
import com.travelbuddy.domain.Session;
import com.travelbuddy.domain.User;
import com.travelbuddy.processors.config.AbstractRunnableProcessor;

import java.util.Calendar;

public class LoginUserProcessor extends AbstractRunnableProcessor<Session> {
    private User user;

    public LoginUserProcessor(ContextContainer context,  User user) {
        super(context);
        this.user = new User();
        this.user.setEmail(user.getEmail());
        this.user.setPassword(user.getPassword());
    }

    @Override
    protected void execute() {

        GetUserCommand.Params params = new GetUserCommand.Params();
        params.setEmail(user.getEmail());
        params.setPassword(user.getPassword());

        GetUserCommand command = new GetUserCommand(params);
        command.execute();

        Session session = new Session();

        session.setUser(command.getUser());
        session.setCalendar(Calendar.getInstance());
        session.setToken(TokenGenerator.generateToken(session));

        this.getDeferredResult().setResult(session);
    }
}