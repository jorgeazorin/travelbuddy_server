package com.travelbuddy.processors.user;

import com.travelbuddy.config.thread.ContextContainer;
import com.travelbuddy.dao.db.commands.user.CreateUserCommand;
import com.travelbuddy.dao.db.commands.user.UpdateUserCommand;
import com.travelbuddy.domain.User;
import com.travelbuddy.processors.config.AbstractRunnableProcessor;

public class UpdateUserProcessor extends AbstractRunnableProcessor<User> {

    private User user;
    public UpdateUserProcessor(ContextContainer context, User user) {
        super(context);
        this.user = user;
    }

    @Override
    protected void execute() {
        user.setId(context.getSession().getUser().getId());
        UpdateUserCommand updateUserCommand = new UpdateUserCommand(user);
        updateUserCommand.execute();

        this.getDeferredResult().setResult(user);
    }
}
