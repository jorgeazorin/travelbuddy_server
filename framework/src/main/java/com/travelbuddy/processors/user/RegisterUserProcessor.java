package com.travelbuddy.processors.user;

import com.travelbuddy.config.thread.ContextContainer;
import com.travelbuddy.dao.db.commands.user.CreateUserCommand;
import com.travelbuddy.domain.User;
import com.travelbuddy.processors.config.AbstractRunnableProcessor;

public class RegisterUserProcessor extends AbstractRunnableProcessor<User> {

    private User user;
    public RegisterUserProcessor(ContextContainer context, User user) {
        super(context);
        this.user = user;
    }

    @Override
    protected void execute() {
        CreateUserCommand createUserCommand = new CreateUserCommand(user);
        createUserCommand.execute();

        this.getDeferredResult().setResult(user);
    }
}
