package com.travelbuddy.processors.buddy;


import com.travelbuddy.config.thread.ContextContainer;
import com.travelbuddy.dao.db.commands.buddy.GetBuddyCommand;
import com.travelbuddy.dao.db.commands.user.GetUserCommand;
import com.travelbuddy.domain.Buddy;
import com.travelbuddy.domain.User;
import com.travelbuddy.processors.config.AbstractRunnableProcessor;

import java.util.List;

public class GetBuddiesProcessor extends AbstractRunnableProcessor<List<Buddy>> {

    private GetBuddyCommand.Params params;
    public GetBuddiesProcessor(ContextContainer context, GetBuddyCommand.Params params) {
        super(context);
        this.params = params;
    }

    @Override
    protected void execute() {
        GetBuddyCommand command = new GetBuddyCommand(params);
        command.execute();
        this.getDeferredResult().setResult(command.getBuddies());
    }
}