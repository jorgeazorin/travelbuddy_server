package com.travelbuddy.processors.buddy;

import com.travelbuddy.config.exceptions.MessageException;
import com.travelbuddy.config.thread.ContextContainer;
import com.travelbuddy.dao.db.commands.buddy.CreateBuddyCommand;
import com.travelbuddy.dao.db.commands.buddy.GetBuddyCommand;
import com.travelbuddy.dao.files.FileManager;
import com.travelbuddy.domain.Buddy;
import com.travelbuddy.processors.config.AbstractRunnableProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public class BeBuddyProcessor extends AbstractRunnableProcessor<Buddy> {

    private Buddy buddy;
    private MultipartFile file;

    public BeBuddyProcessor(ContextContainer context, Buddy buddy, MultipartFile file) {
        super(context);
        this.buddy = buddy;
        this.file = file;
    }

    @Override
    protected void execute() {
        buddy.setUser(this.context.getSession().getUser());

        GetBuddyCommand getBuddyCommand = new GetBuddyCommand(context.getSession().getUser());
        getBuddyCommand.execute();

        Buddy buddy1 = null;
        try{
            buddy1 = getBuddyCommand.getBuddy();
        }catch (Exception e){

        }
        if(buddy1 == null) {
            CreateBuddyCommand createBuddyCommand = new CreateBuddyCommand(buddy);
            createBuddyCommand.execute();
            this.getDeferredResult().setResult(buddy);
        }else{
            throw  new MessageException("This user is already a buddy");
        }

        try {
            FileManager.saveImage(FileManager.Type.IMAGE_BUDDY, file.getInputStream(), context.getSession().getUser().getId());
        }catch (Exception e){

        }

    }
}
