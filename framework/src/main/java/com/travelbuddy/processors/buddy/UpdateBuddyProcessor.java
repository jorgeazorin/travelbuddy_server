package com.travelbuddy.processors.buddy;

import com.travelbuddy.config.thread.ContextContainer;
import com.travelbuddy.dao.db.commands.buddy.UpdateBuddyCommand;
import com.travelbuddy.domain.Buddy;
import com.travelbuddy.processors.config.AbstractRunnableProcessor;

public class UpdateBuddyProcessor extends AbstractRunnableProcessor<Buddy> {

    private Buddy buddy;
    public UpdateBuddyProcessor(ContextContainer context, Buddy buddy) {
        super(context);
        this.buddy = buddy;
    }

    @Override
    protected void execute() {
        buddy.setUser(this.context.getSession().getUser());

        UpdateBuddyCommand updateBuddyCommand = new UpdateBuddyCommand(buddy);
        updateBuddyCommand.execute();
        this.getDeferredResult().setResult(buddy);

    }
}
