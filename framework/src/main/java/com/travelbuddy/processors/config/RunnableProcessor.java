package com.travelbuddy.processors.config;

import org.springframework.web.context.request.async.DeferredResult;

public interface RunnableProcessor<T> extends Runnable {

	public DeferredResult<T> getDeferredResult();

}
