package com.travelbuddy.processors.config;

import com.travelbuddy.dao.db.config.OracleThreadContext;
import com.travelbuddy.config.thread.ThreadContext;
import org.springframework.web.context.request.async.DeferredResult;

import com.travelbuddy.config.thread.ContextContainer;

public abstract class AbstractRunnableProcessor<T> implements RunnableProcessor<T> {

	private static final long DEFERRED_RESULT_TIMEOUT = 300000; // 5 minutes
	
	protected ContextContainer context;

	@SuppressWarnings("unused")
	private AbstractRunnableProcessor() {
	}
	
	public AbstractRunnableProcessor(final ContextContainer context) {
		this.context = context;
	}
	
	private DeferredResult<T> deferredResult = new DeferredResult<T>(DEFERRED_RESULT_TIMEOUT);
	
	@Override
	public void run() {
		final ThreadContext threadContext = (ThreadContext) ThreadContext.getInstance();
		threadContext.copyContainer(context);
		try {
			this.execute();
		} catch (Exception e) {
			this.getDeferredResult().setErrorResult(e);
			if (threadContext.isFromOrchestrator()) {
			    throw e;
			}
		} finally {
			OracleThreadContext.close();
		}
	}
	
	protected abstract void execute();
	
	public DeferredResult<T> getDeferredResult() {
		return deferredResult;
	}

}
