package com.travelbuddy.processors.config;

import com.travelbuddy.config.thread.ContextContainer;


public class CreateContactsProcessor extends AbstractRunnableProcessor<String> {


    public CreateContactsProcessor(ContextContainer context) {
        super(context);
    }

    @Override
    protected void execute() {
        this.getDeferredResult().setResult("ok");
    }
}