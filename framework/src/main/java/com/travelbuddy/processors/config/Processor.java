package com.travelbuddy.processors.config;

import java.util.Map;

public interface Processor {
	public void execute();

	public void putParameter(final String key, final Object value);

	public void putParameter(final String key, final Object value,
			final Map<String, Object> context);

	public <U> U getParameter(final String key);

	public <U> U getParameter(final String key,
			final Map<String, Object> context);
}
