package com.travelbuddy.controllers;

import com.travelbuddy.config.exceptions.MessageException;
import com.travelbuddy.config.mappers.DimitriObjectMapper;
import com.travelbuddy.config.security.UserLogged;
import com.travelbuddy.dao.db.commands.buddy.GetBuddyCommand;
import com.travelbuddy.dao.files.FileManager;
import com.travelbuddy.domain.Buddy;
import com.travelbuddy.processors.buddy.GetBuddiesProcessor;
import com.travelbuddy.processors.buddy.BeBuddyProcessor;
import com.travelbuddy.processors.buddy.UpdateBuddyProcessor;
import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;

@Controller
@RequestMapping("/buddies")
public class BuddiesController extends BaseController {

    @UserLogged
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DeferredResult<List<Buddy>> getBuddies(GetBuddyCommand.Params params) {
        final GetBuddiesProcessor processor = new GetBuddiesProcessor(super.getThreadContext(), params);
        super.run(processor);
        return processor.getDeferredResult();
    }

    @UserLogged
    @RequestMapping( method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DeferredResult<Buddy> updateBuddy(@RequestBody Buddy buddy) {
        final UpdateBuddyProcessor processor = new UpdateBuddyProcessor(super.getThreadContext(), buddy);
        super.run(processor);
        return processor.getDeferredResult();
    }

    @UserLogged
    @RequestMapping( method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public DeferredResult<Buddy> createBuddy(@RequestParam("photo") MultipartFile photo, @RequestParam(value = "buddy", required = false) String buddyString)  {
        DimitriObjectMapper dimitriObjectMapper = new DimitriObjectMapper();
        Buddy buddy;
        try {
            buddy = dimitriObjectMapper.readValue(buddyString, Buddy.class);
        }catch (Exception e){
            throw new RuntimeException("Buddy format error");
        }

        final BeBuddyProcessor processor = new BeBuddyProcessor(super.getThreadContext(), buddy, photo);
        super.run(processor);
        return processor.getDeferredResult();
    }


    @RequestMapping(value="img/{id}", method = RequestMethod.GET)
    public void getImage(@RequestParam Integer id, HttpServletResponse response) {
        InputStream i = FileManager.getImage(FileManager.Type.IMAGE_BUDDY, id);
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        try {
            IOUtils.copy(i, response.getOutputStream());
        }catch (Exception e){
            throw new MessageException("No image");
        }
    }





}