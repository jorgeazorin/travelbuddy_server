package com.travelbuddy.controllers;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.travelbuddy.domain.Session;
import com.travelbuddy.domain.User;
import com.travelbuddy.config.exceptions.MessageException;
import com.travelbuddy.config.exceptions.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import com.travelbuddy.config.contexts.Contexts;
import com.travelbuddy.processors.config.RunnableProcessor;
import com.travelbuddy.config.thread.ContextContainer;


public class BaseController {
	
	@Autowired
	private RequestMappingHandlerMapping requestMappingHandlerMapping;

    @Autowired
    private ServletContext context;


    protected void run(final RunnableProcessor<?> runnableProcessor) {
    	new Thread(runnableProcessor).start();
    }
    
    protected ContextContainer getThreadContext() {

    	ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

    	String ipAddress = attr.getRequest().getHeader("X-Real-IP");
        if (ipAddress == null) {
            ipAddress = attr.getRequest().getRemoteAddr();
        }
        
        final ContextContainer context = new ContextContainer();
        context.setSession(this.getSession());

        context.setUserAgent(attr.getRequest().getHeader("User-Agent"));
        context.setIpAddress(ipAddress);

        context.setRestMapping(this.requestMappingHandlerMapping);
        context.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(this.context));
        
        return context;
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({ UnauthorizedException.class })
    @ResponseBody
    public String handleUnauthorizedException(final Exception ex) {
        return ex.getMessage();
    }


    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ExceptionHandler({ MessageException.class })
    @ResponseBody
    public String handleMessageException(final Exception ex) {
        return ex.getMessage();
    }

    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ExceptionHandler({ RuntimeException.class })
    @ResponseBody
    public String handleRuntimeeException(final Exception ex) {
        return ex.getMessage();
    }

    protected Session getSession() {
    	return (Session) this.getRequest().getAttribute(Contexts.SESSION);
    }


    private HttpServletRequest getRequest() {
    	return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }
}
