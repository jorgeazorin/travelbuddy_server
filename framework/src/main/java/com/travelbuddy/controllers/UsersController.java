package com.travelbuddy.controllers;

import com.travelbuddy.config.security.UserLogged;
import com.travelbuddy.dao.db.commands.user.GetUserCommand;
import com.travelbuddy.domain.Session;
import com.travelbuddy.domain.User;
import com.travelbuddy.processors.user.GetUsersProcessor;
import com.travelbuddy.processors.user.LoginUserProcessor;
import com.travelbuddy.processors.user.RegisterUserProcessor;
import com.travelbuddy.processors.user.UpdateUserProcessor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController extends BaseController {


    @RequestMapping( value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DeferredResult<User> registerUser(@RequestBody User registro) {
        final RegisterUserProcessor processor = new RegisterUserProcessor(super.getThreadContext(), registro);
        super.run(processor);
        return processor.getDeferredResult();
    }



    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DeferredResult<Session> loginUser(@RequestBody User user) {
        LoginUserProcessor loginUserProcessor = new LoginUserProcessor(super.getThreadContext(), user);
        super.run(loginUserProcessor);
        return loginUserProcessor.getDeferredResult();
    }





    @UserLogged
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DeferredResult<List<User>> getUsers(GetUserCommand.Params params) {
        final GetUsersProcessor processor = new GetUsersProcessor(super.getThreadContext(), params);
        super.run(processor);
        return processor.getDeferredResult();
    }

    @UserLogged
    @RequestMapping( method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DeferredResult<User> updateUser(@RequestBody User user) {
        UpdateUserProcessor updateUserProcessor = new UpdateUserProcessor(super.getThreadContext(), user);
        super.run(updateUserProcessor);
        return updateUserProcessor.getDeferredResult();
    }

}