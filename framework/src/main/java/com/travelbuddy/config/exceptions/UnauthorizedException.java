package com.travelbuddy.config.exceptions;


@SuppressWarnings("serial")
public class UnauthorizedException extends RuntimeException {


    public UnauthorizedException(String message) {
        super(message);
    }

    public UnauthorizedException(Exception e) {
        super(e);
    }

}
