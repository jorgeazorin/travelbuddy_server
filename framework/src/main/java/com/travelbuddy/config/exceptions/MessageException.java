package com.travelbuddy.config.exceptions;


@SuppressWarnings("serial")
public class MessageException extends RuntimeException {


    public MessageException(String message) {
        super(message);
    }

    public MessageException(Exception e) {
        super(e);
    }

}
