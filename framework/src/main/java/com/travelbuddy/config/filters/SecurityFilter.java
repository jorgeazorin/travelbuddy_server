package com.travelbuddy.config.filters;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.travelbuddy.config.contexts.Contexts;
import com.travelbuddy.domain.Session;
import com.travelbuddy.config.security.TokenGenerator;

public class SecurityFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        this.doInternalFilter((HttpServletRequest) request, (HttpServletResponse) response, filterChain);
    }

    private void doInternalFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        final String token = retrieveToken(request);
        if (token == null) {
            filterChain.doFilter(request, response);
            return;
        }
        Session session = null;
        try {
            session = retrieveSession(request, token);
        }catch (Exception e){

        }
        if (session != null) {
        	request.setAttribute(Contexts.SESSION, session);
        }

        filterChain.doFilter(request, response);
    }



    private Session retrieveSession(HttpServletRequest request, final String token) {

        Session s = TokenGenerator.getObjectToken(token, Session.class);
        return s;
    }

    @SuppressWarnings("unchecked")
    private String retrieveToken(HttpServletRequest request) {
        final Enumeration<String> headers = (Enumeration<String>) request.getHeaders("Authorization");
        while (headers.hasMoreElements()) {
            final String header = headers.nextElement();
            if (header.startsWith("Bearer ")) {
                return header.substring(7);
            }else{
                return header;
            }
        }
        return null;
    }

    public void destroy() {
    }

    public void init(FilterConfig arg0) throws ServletException {
    }

}
