package com.travelbuddy.config.thread;

import com.travelbuddy.domain.Session;
import com.travelbuddy.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

public class ThreadContext implements Context {

	private ContextContainer container = new ContextContainer();
	
	private static ThreadLocal<ThreadContext> instance = new ThreadLocal<ThreadContext>(){
        @Override
        protected ThreadContext initialValue() {
            return new ThreadContext();
        }
    };
    
    public static Context getInstance() {
        return instance.get();
    }
    
    public void copyContainer(final ContextContainer container) {
    	this.container = container;
    }

	public Session getSession() {
		return container.getSession();
	}

	public void setSession(Session session) {
		container.setSession(session);
	}

	public String getIpAddress() {
		return container.getIpAddress();
	}



	public RequestMappingHandlerMapping getRestMapping() {
		return container.getRestMapping();
	}


	public ApplicationContext getApplicationContext() {
		return container.getApplicationContext();
	}

	public boolean isFromOrchestrator() {
	    return container.fromOrchestrator;
	}
	

}
