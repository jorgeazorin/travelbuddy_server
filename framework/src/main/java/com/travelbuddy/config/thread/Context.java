package com.travelbuddy.config.thread;

import com.travelbuddy.domain.Session;
import com.travelbuddy.domain.User;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

public interface Context {

	Session getSession();

	RequestMappingHandlerMapping getRestMapping();

}
