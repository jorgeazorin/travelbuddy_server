package com.travelbuddy.config.thread;

import com.travelbuddy.domain.Session;
import com.travelbuddy.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;


public class ContextContainer {

	private Session session;
	private String ipAddress;
	private String userAgent;
	private  RequestMappingHandlerMapping restMapping;
	private ApplicationContext applicationContext;
	boolean fromOrchestrator;

	
	public void setSession(Session session) {
		this.session = session;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public void setRestMapping(RequestMappingHandlerMapping restMapping) {
		this.restMapping = restMapping;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

    public void setFromOrchestrator(boolean fromOrchestrator) {
        this.fromOrchestrator = fromOrchestrator;
    }

    public String getIpAddress(){
		return this.ipAddress;
	}
	public Session getSession() {return  this.session;}

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public RequestMappingHandlerMapping getRestMapping() {
        return restMapping;
    }
}
