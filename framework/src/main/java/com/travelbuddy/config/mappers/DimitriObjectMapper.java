package com.travelbuddy.config.mappers;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

@SuppressWarnings("serial")
public class DimitriObjectMapper extends ObjectMapper {

    public DimitriObjectMapper() {
        super();
        super.setSerializationInclusion(Include.NON_NULL);
        super.registerModule(new JodaModule());
        super.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS , false);
    }
}
