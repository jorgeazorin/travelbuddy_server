package com.travelbuddy.config.interceptors;

import com.travelbuddy.config.exceptions.UnauthorizedException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import com.travelbuddy.config.contexts.Contexts;

@Aspect
public class ControllerInterceptor {

    public void checkToken(){
        if (Contexts.getSession() == null)
            throw new UnauthorizedException("Acceso no permitido");
    }


    @Before("@annotation(com.travelbuddy.config.security.UserLogged)")
    public void checkUserLogged(final JoinPoint joinPoint) {

        checkToken();
    }
}
