package com.travelbuddy.config.contexts;

import com.travelbuddy.domain.Session;
import com.travelbuddy.domain.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class Contexts {


    public static final String SESSION = "_USER_SESSION_";

	public static Session getSession() {
	    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return (Session) attr.getRequest().getAttribute(SESSION);
	}
}
