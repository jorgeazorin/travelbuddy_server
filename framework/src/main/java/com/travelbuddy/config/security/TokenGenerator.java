package com.travelbuddy.config.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.travelbuddy.config.mappers.DimitriObjectMapper;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;

public class TokenGenerator {
    private static final String SECRETO = "Un secretjko jamas contado es como una clave chula jajaja */*/-*/-*/ MIERDA" +
            "";

    public static String generateToken(Object o){
        try {
            DimitriObjectMapper dimitriObjectMapper = new DimitriObjectMapper();
            String s = dimitriObjectMapper.writeValueAsString(o);

            Algorithm algorithm = Algorithm.HMAC256(SECRETO);
            String token = JWT.create().withSubject(s).withIssuer("auth0").sign(algorithm);

            return token;
        } catch (UnsupportedEncodingException exception){
            throw new RuntimeException(exception);
        } catch (JWTCreationException exception){
            throw new RuntimeException(exception);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public static String generateToken(Object o, Calendar expireDate){
        try {
            DimitriObjectMapper dimitriObjectMapper = new DimitriObjectMapper();
            String s = dimitriObjectMapper.writeValueAsString(o);

            Algorithm algorithm = Algorithm.HMAC256(SECRETO);
            String token = JWT.create().withExpiresAt(expireDate.getTime()).withSubject(s).withIssuer("auth0").sign(algorithm);

            return token;
        } catch (UnsupportedEncodingException exception){
            throw new RuntimeException(exception);
        } catch (JWTCreationException exception){
            throw new RuntimeException(exception);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }


    public static <T> T getObjectToken(String token, Class<T> clazz)  {
        DimitriObjectMapper dimitriObjectMapper = new DimitriObjectMapper();
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRETO);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            return dimitriObjectMapper.readValue(jwt.getSubject(), clazz);

        } catch (UnsupportedEncodingException exception){
            throw new RuntimeException(exception);
        } catch (JWTVerificationException exception){
            throw new RuntimeException(exception);
        }catch (Exception e){
            throw  new RuntimeException(e);
        }

    }

}
