package com.travelbuddy.config.security;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.travelbuddy.config.exceptions.MessageException;
import com.travelbuddy.config.mappers.DimitriObjectMapper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    public static String getMD5(String string) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new MessageException("Error cifrando password") ;
        }

        md.update(string.getBytes());
        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();

    }


    public static String getMD5(Object string) throws JsonProcessingException {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new MessageException("Error cifrando password") ;
        }
        DimitriObjectMapper dimitriObjectMapper = new DimitriObjectMapper();
        md.update(dimitriObjectMapper.writeValueAsString(string).getBytes());
        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();

    }

}
