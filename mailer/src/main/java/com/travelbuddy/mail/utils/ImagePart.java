package com.travelbuddy.mail.utils;

import javax.activation.DataHandler;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;

public class ImagePart {
    private String name;
    private String type;
    private MimeBodyPart mimeBodyPart;

    public ImagePart(String name, InputStream stream){
        try {
            mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource( stream, "application/pdf")));
            mimeBodyPart.setFileName(name);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ImagePart(String name, String type, InputStream stream){
        try {
            mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource( stream, type)));
            mimeBodyPart.setHeader("Content-ID", "<"+name+">");
        } catch (Exception e) {
            e.printStackTrace();
        };
    }

    public MimeBodyPart getMimeBodyPart(){
        return mimeBodyPart;
    }
}
