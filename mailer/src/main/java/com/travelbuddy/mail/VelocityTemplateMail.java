package com.travelbuddy.mail;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;

import com.travelbuddy.mail.utils.ImagePart;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.tools.generic.ComparisonDateTool;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.EscapeTool;

public abstract class VelocityTemplateMail {

    private Template template;
    private String users;
    private VelocityContext context = new VelocityContext();
    private String templateName;
    private String subject;
    protected List<ImagePart> imageParts = new ArrayList<>();
    private Boolean cco = false;

    private  void  iniciar(){
        Properties p = new Properties();
        p.setProperty("resource.loader", "class");
        p.setProperty("class.resource.loader.description", "Velocity Classpath Resource Loader");
        p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        p.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");

        Velocity.init(p);

        this.template = Velocity.getTemplate("templates/" + templateName, "UTF-8");
        this.context.put("date", new DateTool());
        this.context.put("comparison", new ComparisonDateTool());
        this.context.put("data", this);
        this.context.put("esc",new EscapeTool());
    }


    public VelocityTemplateMail(String templateName, String users, String subject) {
        this.users = users;
        this.templateName = templateName;
        this.subject = subject;
        this.cco = false;
        iniciar();
    }



    public VelocityTemplateMail(String templateName, String users, String subject, Boolean cco) {
        this.cco = cco;
        this.users = users;
        this.templateName = templateName;
        this.subject = subject;
        iniciar();
    }




    static Properties mailServerProperties;
    static Session getMailSession;



    public void generateAndSendEmail() throws AddressException, MessagingException {

        StringWriter content = new StringWriter();
        Velocity.mergeTemplate("templates/" + templateName, "UTF-8", context, content);

        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");

        getMailSession = Session.getDefaultInstance(mailServerProperties, null);

        Message message = new MimeMessage(getMailSession);

        if(cco){
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("info@fingerink.biz"));
            message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(users));
        }else{
            // message.setFrom(new InternetAddress("no-reply@everis.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(users));
        }
        message.setSubject(subject);

        final MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(content.toString(), "text/html");
        final Multipart mp = new MimeMultipart("alternative");
        mp.addBodyPart(htmlPart);

        if(imageParts!=null)
            for(ImagePart imagePart : imageParts)
                mp.addBodyPart(imagePart.getMimeBodyPart());

        message.setContent(mp);
        Transport transport = getMailSession.getTransport("smtp");
        transport.connect("smtp.gmail.com", "support@fingerink.biz", "ivanqwerty123456");
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
}
