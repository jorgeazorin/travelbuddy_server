package com.travelbuddy.mail;

import com.travelbuddy.mail.utils.ImagePart;

public class ExampleMailer extends VelocityTemplateMail {

    private String texto;
    public ExampleMailer(String users) {
        super("exampleTemplate.vm" , users, "Mail ejemplo");
        this.imageParts.add(new ImagePart("6@2x", "image/png", this.getClass().getResourceAsStream("/images/6@2x.png")));
        this.imageParts.add(new ImagePart("logo", "image/png", this.getClass().getResourceAsStream("/images/logo.png")));
    }

    public String getTexto(){
        return this.texto;
    }
}
